# Necessary tools
1. [Spring Boot CLI](https://docs.spring.io/spring-boot/docs/current/reference/html/cli.html)
2. [Docker](https://docs.docker.com/install/)
3. [Pulumi CLI](https://www.pulumi.com/docs/cli/)
4. [gcloud CLI](https://cloud.google.com/sdk/gcloud)

# Necessary accounts
1. [GitLab](https://gitlab.com/)
2. [Docker Hub](https://hub.docker.com/)
3. [Pulumi Cloud](https://www.pulumi.com/)
4. [Google Cloud Platform](https://cloud.google.com/)

# GitLab Setup
1. Execute `docker login` locally and share your `~/.docker/config.json` as [GitLab's environment variable](https://gitlab.com/martin.dulak/javadays-2023/-/settings/ci_cd) `DOCKER_AUTH_CONFIG`.
2. Generate [Pulumi Access Token](https://app.pulumi.com/martin.dulak/settings/tokens) and set it as GitLab env. variable `PULUMI_ACCESS_TOKEN`.
