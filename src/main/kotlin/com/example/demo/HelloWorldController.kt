package com.example.demo

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class HelloWorldController {
    @GetMapping("/hello")
    fun helloWorld(): String {
        return "Hello JavaDays! Uploaded by Pulumi!"
    }
}
