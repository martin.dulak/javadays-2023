import * as pulumi from "@pulumi/pulumi";
import * as gcp from "@pulumi/gcp";

// if you don't have an organization, you must create the project manually
const project = new gcp.organizations.Project("project", {
    orgId: "ORG_ID", // replace with your org id
    projectId: "java-days-2023",
    billingAccount: "BILLING_ACCOUNT_ID", // replace with your billing account id
});

const cloudRunService = new gcp.projects.Service("cloud-run", {
    project: project.projectId,
    service: "run.googleapis.com",
});

const backendService = new gcp.cloudrunv2.Service("backend", {
    project: project.projectId,
    location: "europe-west3",
    template: {
        containers: [
            {
                // replace with your tag
                image: pulumi.interpolate`martin7/java-days:${new pulumi.Config().require("version")}`,
            }
        ]
    }
}, {dependsOn: cloudRunService});

new gcp.cloudrun.IamBinding("my-iam-binding", {
    location: "europe-west3",
    project: project.projectId,
    service: backendService.name,
    role: "roles/run.invoker",
    members: ["allUsers"],
});

const sa = new gcp.serviceaccount.Account("gitlab", {
    project: project.projectId,
    accountId: "gitlab",
});

new gcp.projects.IAMBinding("project-owners", {
    project: project.projectId,
    role: "roles/owner",
    members: [
        "martin.dulak@morosystems.cz", // replace with your account to not lose access to the project
        pulumi.interpolate`serviceAccount:${sa.email}`,
    ],
});

const saKey = new gcp.serviceaccount.Key("gitlab-key", {
    serviceAccountId: sa.name,
});

export const backendUrl = backendService.uri;
export const serviceAccountKey = saKey.privateKey;
